package com.maxbill.fxui.head;

import javafx.application.Platform;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import static com.maxbill.fxui.body.ConfWindow.initConfTab;
import static com.maxbill.fxui.body.InfoWindow.initInfoTab;
import static com.maxbill.fxui.body.LogsWindow.initLogsTab;
import static com.maxbill.fxui.body.WarnWindow.initWarnTab;
import static com.maxbill.fxui.util.CommonConstant.*;
import static com.maxbill.fxui.util.DesktopObjects.*;

/**
 * 头部窗口
 *
 * @author MaxBill
 * @date 2019/07/06
 */
public class HeadWindow {

    private static GridPane headPane;
    private static GridPane menuPane;

    private static VBox menuItem01;
    private static VBox menuItem02;
    private static VBox menuItem03;
    private static VBox menuItem04;
    private static VBox menuItem05;
    private static VBox menuItem06;
    private static VBox menuItem07;


    /**
     * 创建头部窗口
     */
    public static BorderPane getHeadWindow() {
        getHeadPane();
        getMenuPane();
        BorderPane headWindow = new BorderPane();
        headWindow.setId("head-pane");
        headWindow.setPrefHeight(100);
        headWindow.getStylesheets().add(STYLE_HEAD);
        headWindow.setTop(headPane);
        headWindow.setBottom(menuPane);
        headWin = headWindow;
        return headWindow;
    }

    /**
     * 创建头部标题栏
     */
    private static void getHeadPane() {
        headPane = new GridPane();
        headPane.setId("head-view");
        headPane.setHgap(10);

        Label headImage = new Label();
        Label headTitle = new Label();
        Label headItems = new Label();
        Label headAbate = new Label();
        Label headRaise = new Label();
        Label headClose = new Label();

        headTitle.setText(APP_NAME);
        headImage.setId("head-view-image");
        headTitle.setId("head-view-title");
        headItems.setId("head-view-items");
        headAbate.setId("head-view-abate");
        headRaise.setId("head-view-raise");
        headClose.setId("head-view-close");

        headImage.setPrefSize(27, 23);
        headItems.setPrefSize(27, 23);
        headAbate.setPrefSize(27, 23);
        headRaise.setPrefSize(27, 23);
        headClose.setPrefSize(27, 23);

        headPane.add(headImage, 0, 0);
        headPane.add(headTitle, 1, 0);
        headPane.add(headItems, 2, 0);
        headPane.add(headAbate, 3, 0);
        headPane.add(headRaise, 4, 0);
        headPane.add(headClose, 5, 0);

        headPane.setPadding(new Insets(5));
        headPane.setAlignment(Pos.CENTER_LEFT);
        GridPane.setHgrow(headTitle, Priority.ALWAYS);

        //事件监听
        //1.监听操作选项事件
        headItems.setOnMouseClicked(event -> doWinItems());
        //2.监听窗口最小事件
        headAbate.setOnMouseClicked(event -> doWinAbate());
        //3.监听窗口最大事件
        headRaise.setOnMouseClicked(event -> doWinRaise());
        //4.监听窗口关闭事件
        headClose.setOnMouseClicked(event -> doWinClose());

    }

    /**
     * 创建头部菜单栏
     */
    private static void getMenuPane() {
        menuPane = new GridPane();
        menuPane.setId("menu-view");
        menuPane.setHgap(10);
        menuPane.setPrefHeight(68);

        //定义菜单项
        menuItem01 = getMenuItem(MENU_ICON_01, " 服务连接");
        menuItem02 = getMenuItem(MENU_ICON_02, " 服务信息");
        menuItem03 = getMenuItem(MENU_ICON_03, " 服务配置");
        menuItem04 = getMenuItem(MENU_ICON_04, " 服务监控");
        menuItem05 = getMenuItem(MENU_ICON_05, " 命令终端");
        menuItem06 = getMenuItem(MENU_ICON_06, " 日志信息");
        menuItem07 = getMenuItem(MENU_ICON_07, " 系统设置");

        //添加菜单项
        menuPane.add(menuItem01, 0, 0);
        menuPane.add(menuItem02, 1, 0);
        menuPane.add(menuItem03, 2, 0);
        menuPane.add(menuItem04, 3, 0);
        menuPane.add(menuItem05, 4, 0);
        menuPane.add(menuItem06, 5, 0);
        menuPane.add(menuItem07, 6, 0);

        menuItem01.setOnMouseClicked(event -> {
            checkMenuItem(1);
        });

        menuItem02.setOnMouseClicked(event -> {
            checkMenuItem(2);
            initInfoTab();
        });

        menuItem03.setOnMouseClicked(event -> {
            checkMenuItem(3);
            initConfTab();
        });

        menuItem04.setOnMouseClicked(event -> {
            checkMenuItem(4);
            initWarnTab();
        });

        menuItem05.setOnMouseClicked(event -> {
            checkMenuItem(5);
        });

        menuItem06.setOnMouseClicked(event -> {
            checkMenuItem(6);
            initLogsTab();
        });

        menuItem07.setOnMouseClicked(event -> {
            checkMenuItem(7);
        });

        //添加软件LOGO
        ImageView logoImage = new ImageView();
        logoImage.setImage(new Image(APP_LOGO));
        logoImage.setFitWidth(128);
        logoImage.setFitHeight(42);
        Label emptLabel = new Label();
        emptLabel.setPrefWidth(10);
        menuPane.add(logoImage, 7, 0);
        menuPane.add(emptLabel, 8, 0);
        GridPane.setValignment(logoImage, VPos.CENTER);
        GridPane.setHalignment(logoImage, HPos.RIGHT);
        GridPane.setHgrow(logoImage, Priority.ALWAYS);

    }

    /**
     * 监听窗口选项事件
     */
    private static void doWinItems() {

    }

    /**
     * 监听窗口最小事件
     */
    private static void doWinAbate() {
        rootWin.setIconified(true);
    }

    /**
     * 监听窗口最大事件
     */
    private static void doWinRaise() {
        IS_FULL = !IS_FULL;
        rootWin.setMaximized(IS_FULL);
    }

    /**
     * 监听窗口关闭事件
     */
    private static void doWinClose() {
        rootWin.close();
        Platform.exit();
        System.exit(0);
    }

    /**
     * 生成菜单项目
     */
    private static VBox getMenuItem(String imgUrl, String name) {
        VBox menuItem = new VBox();
        menuItem.setPrefSize(70, 68);
        menuItem.getStyleClass().add("menu-item");
        menuItem.setSpacing(5);
        menuItem.setAlignment(Pos.CENTER);
        ImageView menuImage = new ImageView();
        menuImage.setImage(new Image(imgUrl));
        menuImage.setFitWidth(30);
        menuImage.setFitHeight(30);
        menuItem.getChildren().add(menuImage);
        Label menuName = new Label(name);
        menuName.setId("menu-name");
        menuItem.getChildren().add(menuName);
        return menuItem;
    }

    /**
     * 菜单点击切换状态
     *
     * @param type 菜单类型
     */
    private static void checkMenuItem(int type) {
        menuItem01.getStyleClass().removeAll("menu-view-curr");
        menuItem02.getStyleClass().removeAll("menu-view-curr");
        menuItem03.getStyleClass().removeAll("menu-view-curr");
        menuItem04.getStyleClass().removeAll("menu-view-curr");
        menuItem05.getStyleClass().removeAll("menu-view-curr");
        menuItem06.getStyleClass().removeAll("menu-view-curr");
        menuItem07.getStyleClass().removeAll("menu-view-curr");
        switch (type) {
            case 1:
                menuItem01.getStyleClass().add("menu-view-curr");
                break;
            case 2:
                menuItem02.getStyleClass().add("menu-view-curr");
                break;
            case 3:
                menuItem03.getStyleClass().add("menu-view-curr");
                break;
            case 4:
                menuItem04.getStyleClass().add("menu-view-curr");
                break;
            case 5:
                menuItem05.getStyleClass().add("menu-view-curr");
                break;
            case 6:
                menuItem06.getStyleClass().add("menu-view-curr");
                break;
            case 7:
                menuItem07.getStyleClass().add("menu-view-curr");
                break;
            default:
                break;
        }
    }

}
