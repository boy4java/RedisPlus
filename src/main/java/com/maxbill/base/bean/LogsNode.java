package com.maxbill.base.bean;

import lombok.Data;
import redis.clients.jedis.util.Slowlog;

import java.util.ArrayList;
import java.util.List;

import static com.maxbill.util.DateUtil.unixTimeStampToDateTime;

@Data
public class LogsNode {

    private String logId;
    private String logTime;
    private String exeTime;
    private String logArgs;

    private static LogsNode copyToLogsNode(Slowlog slowlog) {
        LogsNode node = new LogsNode();
        node.setLogId(String.valueOf(slowlog.getId()));
        node.setLogTime(unixTimeStampToDateTime(slowlog.getTimeStamp()));
        node.setExeTime(String.valueOf(slowlog.getExecutionTime()));
        node.setLogArgs(String.join(",", slowlog.getArgs()));
        return node;
    }


    public static List<LogsNode> copyToLogsNode(List<Slowlog> logList) {
        List<LogsNode> nodeList = new ArrayList<>();
        if (null == logList || logList.isEmpty()) {
            return nodeList;
        }
        logList.forEach(slowlog -> {
            nodeList.add(copyToLogsNode(slowlog));
        });
        return nodeList;
    }

}
