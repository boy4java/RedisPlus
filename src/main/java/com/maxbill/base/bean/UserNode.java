package com.maxbill.base.bean;

import lombok.Data;

@Data
public class UserNode {

    private String id;

    private String host;

    private String time;

    private String db;

}
